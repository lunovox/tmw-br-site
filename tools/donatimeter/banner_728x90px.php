<?php
	$URL_Fundo="elementos/fundo.png";
	$URL_logo="elementos/logotipo.png";
	$URL_conteiner="elementos/conteiner.png";
	$URL_IconRun1="elementos/icon-tmw.png";
	$URL_IconRun2="elementos/icon-clock.png";
	$URL_antitedio="elementos/antitedio.png";
	$URL_vitory="elementos/icon-smile.png";
	$URL_falling="elementos/icon-cave.png";
	$URL_anuncio="elementos/dizeres.png";
	
	$Bar1_UI=getValor("moeda");
	$Bar1_Now=getValor("doado");
	$Bar1_Max=getValor("custo");

	$Bar2_Min=getValor("inicio");
	$Bar2_Max=getValor("termino");
	$Bar2_Now=date("Y-m-d");
	$Bar2_UI="DIAS";
	
	$Bar_Alpha=70; //de 0 atpe 127
	$Bar_Fonte="elementos/Verdana_Bold.ttf";
	$Bar_FontSize=10;
	
	$GD=@gd_info();
	if(is_array($GD)==1){
		if($GD["PNG Support"]=1){
			header("content-type:image/png");
			
			$bg=imagecreatefrompng($URL_Fundo);
			imagealphablending($bg,true);

			$logo=imagecreatefrompng($URL_logo);
			$size=getimagesize($URL_logo);
			imagecopyresampled($bg,$logo,5,5,0,0,$size[0],$size[1],$size[0],$size[1]);

			$antitedio=imagecreatefrompng($URL_antitedio);
			$size=getimagesize($URL_antitedio);
			imagecopyresampled($bg,$antitedio,90,60,0,0,$size[0],$size[1],$size[0],$size[1]);

			$vitory=imagecreatefrompng($URL_vitory);
			$size=getimagesize($URL_vitory);
			imagecopyresampled($bg,$vitory,445,3,0,0,$size[0],$size[1],$size[0],$size[1]);

			$falling=imagecreatefrompng($URL_falling);
			$size=getimagesize($URL_falling);
			imagecopyresampled($bg,$falling,445,33,0,0,$size[0],$size[1],$size[0],$size[1]);

			$falling=imagecreatefrompng($URL_falling);
			$size=getimagesize($URL_falling);
			imagecopyresampled($bg,$falling,445,33,0,0,$size[0],$size[1],$size[0],$size[1]);

			$anuncio=imagecreatefrompng($URL_anuncio);
			$size=getimagesize($URL_anuncio);
			imagecopyresampled($bg,$anuncio,480,10,0,0,$size[0],$size[1],$size[0],$size[1]);

			$conteiner=imagecreatefrompng($URL_conteiner);
			$size=getimagesize($URL_conteiner);
			imagecopyresampled($bg,$conteiner,120,5,0,0,$size[0],$size[1],$size[0],$size[1]);
			imagecopyresampled($bg,$conteiner,120,35,0,0,$size[0],$size[1],$size[0],$size[1]);
			
			if($Bar_Alpha<0){$Bar_Alpha=0;}elseif($Bar_Alpha>127){$Bar_Alpha=127;}
			if($Bar1_Now<0){$Bar1_Now=0;}elseif($Bar1_Now>$Bar1_Max){$Bar1_Now=$Bar1_Max;}
			$Branco=imagecolorallocate($bg,255,255,255);

			$bar1=imagecolorallocatealpha($bg,0,255,0,$Bar_Alpha);
			imagefilledrectangle($bg,121,6,118+($size[0]*($Bar1_Now/$Bar1_Max)),22,$bar1);
			$IconRun1=imagecreatefrompng($URL_IconRun1);
			$size_IconRun1=getimagesize($URL_IconRun1);
			imagecopyresampled($bg,$IconRun1,107+($size[0]*($Bar1_Now/$Bar1_Max)),3,0,0,$size_IconRun1[0],$size_IconRun1[1],$size_IconRun1[0],$size_IconRun1[1]);
			$TText=($Bar1_Now)."/".$Bar1_Max."(".$Bar1_UI.")";
			$TBox=imagettfbbox($Bar_FontSize,0,$Bar_Fonte,$TText);
			$TBoxLarg=$TBox[4]-$TBox[0];
			if($Bar1_Now/$Bar1_Max>0.5){
				imagettftext($bg,$Bar_FontSize,0,128,19,$Branco,$Bar_Fonte,$TText);/**/
			}else{
				imagettftext($bg,$Bar_FontSize,0,118+$size[0]-$TBoxLarg,19,$Branco,$Bar_Fonte,$TText);/**/
			}


			$Bar2b_Multiplo=60*60*24;
			$Bar2b_Min=(int)DataSQLparaMaxInteiroPHP($Bar2_Min);
			if($Bar2b_Min<0){$Bar2b_Min=0;}
			$Bar2b_Now=(int)((DataSQLparaMaxInteiroPHP($Bar2_Now)-$Bar2b_Min)/$Bar2b_Multiplo);
			$Bar2b_Max=(int)((DataSQLparaMaxInteiroPHP($Bar2_Max)-$Bar2b_Min)/$Bar2b_Multiplo);
			if($Bar2b_Max<0){$Bar2b_Max=0;}
			if($Bar2b_Now<0){$Bar2b_Now=0;}elseif($Bar2b_Now>$Bar2b_Max){$Bar2b_Now=$Bar2b_Max;}
			$Bar2b_UI=$Bar2_UI;

			$bar2=imagecolorallocatealpha($bg,255,0,0,$Bar_Alpha);
			imagefilledrectangle($bg,121,36,118+($size[0]*($Bar2b_Now/$Bar2b_Max)),52,$bar2);
			$IconRun2=imagecreatefrompng($URL_IconRun2);
			$size_IconRun2=getimagesize($URL_IconRun2);
			imagecopyresampled($bg,$IconRun2,107+($size[0]*($Bar2b_Now/$Bar2b_Max)),33,0,0,$size_IconRun2[0],$size_IconRun2[1],$size_IconRun2[0],$size_IconRun2[1]);

			$TText=($Bar2b_Now)."/".$Bar2b_Max."(".$Bar2b_UI.")";
			$TBox=imagettfbbox($Bar_FontSize,0,$Bar_Fonte,$TText);
			$TBoxLarg=$TBox[4]-$TBox[0];
			if($Bar2b_Now/$Bar2b_Max>0.5){
				imagettftext($bg,$Bar_FontSize,0,128,49,$Branco,$Bar_Fonte,$TText);/**/
			}else{
				imagettftext($bg,$Bar_FontSize,0,118+$size[0]-$TBoxLarg,49,$Branco,$Bar_Fonte,$TText);/**/
			}


			imagepng($bg);
			imagedestroy($bg);
		}else{
			echo "Imagem PNG não é suportada!!!";
		}
	}else{
		echo "Função Gráfica não é suportado!!!";
	}/**/
//-----------------------------------------------------------------------------------
	function Propriedade($NomeDaPropriedade=NULL){
	  if(isset($NomeDaPropriedade) && $NomeDaPropriedade!=""){
			if(isset($_POST[$NomeDaPropriedade]) && $_POST[$NomeDaPropriedade]!=""){
				$Conteudo=$_POST[$NomeDaPropriedade];
			}elseif(isset($_GET[$NomeDaPropriedade]) && $_GET[$NomeDaPropriedade]!=""){
				$Conteudo=$_GET[$NomeDaPropriedade];
			}
			if(isset($Conteudo)){return $Conteudo;}
		}
	}
	function DataSQLparaMaxInteiroPHP($Data=""){
		if($Data!=""){
		  $Parte=explode("-",$Data);
		  if(count($Parte)==3){
				$DataNumerica=(int)mktime(23, 59, 59, $Parte[1], $Parte[2], $Parte[0]);
			}else{$DataNumerica="ERRO";}
		}else{$DataNumerica="";}
		return $DataNumerica;
	}
	function getValor($Atributo=""){
		if($Atributo!=""){
			$Conteudo=file_get_contents("donatemeter.conf");
			if($Conteudo!=""){
				$StrInicio=strpos($Conteudo,$Atributo);
				if($StrInicio>=0){
					$StrFim=strpos($Conteudo,";",$StrInicio+strlen($Atributo));
					if($StrFim>$StrInicio){
						$StrMeio=strpos($Conteudo,"=",$StrInicio+strlen($Atributo));
						if($StrMeio>$StrInicio && $StrMeio<$StrFim){
							return substr($Conteudo,$StrMeio+1,$StrFim-($StrMeio+1));
						}
					}
				}
			}
		}
	}
?>
