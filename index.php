<?php require_once "libs/libGeral.php";?>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>The Mana World - Brasil</title>
		<link href="estilo.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="imagens/icones/favicon.png" />
		<META NAME="description" CONTENT="Site oficial no Brasil do MMORPG Open Source The Mana World">
		<META NAME="keywords" CONTENT="jogo, game, open source, MMORPG, TMW, The Mana World">
		<META NAME="licence" CONTENT="Open Source">
		<LINK REL="shortcut icon" HREF="imagens/icones/favicon.png">
		<META NAME="OWNER" CONTENT="lunovox@tuatec.com.br">
		<LINK REV="made" HREF="lunovox@tuatec.com.br">
		<META NAME="distribution" CONTENT="Global">
		<META NAME="rating" CONTENT="General">
		<META NAME="author" CONTENT="Lunovox">
		<META NAME="content-language" CONTENT="pt-br">
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
		<META HTTP-EQUIV="generator" CONTENT="Quanta Plus + GIMP" />
		<META NAME="robots" CONTENT="index, follow, noimageindex">
		<LINK REL="stylesheet" HREF="estilo.css" TYPE="text/css">
		<script src="libs/libGeral.js"></script>
		<script src="libs/blcNaoSelecionavel.js"></script>
	</head>
	<body>
		<header>
			<!-- header = cabeçalho -->
				<p align="center">
					<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
					<!--audio preload="false" autoplay="false" stop="true" loop="true" src="musicas/Magick_-_Real.ogg" controls="true" align="center"></audio-->
					<div class="botaotwitter" onClick="doTwitter('O jogo [MUNDO DE MANA] é um MMORPG 2D aberto e gratuito muito bacana. XD', 'http://www.themanaworld.com.br', 'Lunovox')"></div>
					<div class="botaocurtir" onClick="doFacebook('http://www.themanaworld.com.br')"></div>
				</p>
		</header>
		<nav>
			<button onClick="window.location='./'">HOME</button>
			<button onClick="location='?sub=videos'">VÍDEOS</button>
			<!--button disabled onClick="location='?sub=fotos'">FOTOS</button-->
			<button onClick="location='?sub=download'">DOWNLOAD</button>
			<button onClick="location='?sub=registrar'">REGISTRAR</button>
			<button disabled onClick="location='?sub=eventOs'">EVENTOS</button>
			<button disabled onClick="location='?sub=wiki'">WIKI</button>
			<!--
				<button disabled onClick="location='?sub=wiki-classes'">CLASSES</button>
				<button disabled onClick="location='?sub=wiki-quests'">QUESTS</button>
				<button disabled onClick="location='?sub=wiki-itens'">ÍTENS</button>
				<button disabled onClick="location='?sub=wiki-inimigos'">INIMIGOS</button>
				<button disabled onClick="location='?sub=wiki-comandos'">COMANDOS</button>
				<button disabled onClick="location='?sub=wiki-codigofonte'">CÓDIGO FONTE</button>
				
				<button disabled onClick="location='?sub=tmwmaker'">TMW-MAKER</button>
				<button disabled onClick="location='?sub=desenvolvimento'">DEV</button>
				<button disabled onClick="location='?sub=artes'">ARTES</button>
			-->
			<button onClick="location='?sub=webchat'">WEBCHAT</button>
			<button onClick="location='?sub=forum'">FÓRUM</button>
			<button onClick="location='?sub=licence'">LICENÇA</button>
			<button onClick="location='?sub=donate'">DOAÇÕES</button>
		</nav>
		<section><!-- section = conteudo -->
<?php 
	$sub=Propriedade("sub")!=""?Propriedade("sub"):"home";
	$url="subs/$sub.php";
	$url=file_exists($url)?$url:"subs/erro.php";
	require_once $url;
?>
		</section>
		<footer>
			<!-- footer = rodapé -->
		</footer>
	</body>
</html>
