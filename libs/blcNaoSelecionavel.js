document.oncontextmenu = function(){return false;};

// ##### Script que impede que o texto seja selecionado quando um widget é arrastado na tela. #############################
function disableSelectTeste(e){
	if(e.target.tagName != "INPUT" && e.target.tagName != "TEXTAREA") {
		return false;
	}
	return true;
}

function enableSelectTeste(){
	return true;
}

function filterSelectability(){
	if(event.srcElement.tagName != "INPUT" && event.srcElement.tagName != "TEXTAREA") {
		return false;
}
return true;
}
document.onselectstart = filterSelectability;
//document.onselectstart = new Function ("return false")
if (window.sidebar){
	document.onmousedown = disableSelectTeste;
	document.onclick = enableSelectTeste;
}
// ########################################################################################################################
